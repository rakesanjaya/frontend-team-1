import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// * Pages yang akan di render
import {
  Login,
  Register,
  PlayerVsCom,
  P1vsP2,
  CreateRoom,
  LobbyGame,
  GameHistory,
  HomePage,
  NotFound,
  StaticPage,
} from "../../pages/AccessAllPages";

function Halaman() {
  return (
    <Router>
      <Routes>
        {/* <Route path="/" element={<HomePage />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/register" element={<Register />}></Route>
        <Route path="/playervscom" element={<PlayerVsCom />}></Route>
        <Route path="/p1vsp2" element={<P1vsP2 />}></Route>
        <Route path="/lobbygame" element={<LobbyGame />}></Route>
        <Route path="/createRoom" element={<CreateRoom />}></Route>
        <Route path="/gamehistory" element={<GameHistory />}></Route>
        <Route path="*" element={<NotFound />} /> */}
        <Route path="/" element={<StaticPage />}>
          <Route path="/" element={<HomePage />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/register" element={<Register />}></Route>
          <Route path="/playervscom" element={<PlayerVsCom />}></Route>
          <Route path="/p1vsp2" element={<P1vsP2 />}></Route>
          <Route path="/lobbygame" element={<LobbyGame />}></Route>
          <Route path="/createRoom" element={<CreateRoom />}></Route>
          <Route path="/gamehistory" element={<GameHistory />}></Route>
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </Router>
  );
}

export default Halaman;
