/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useState, useEffect } from "react";
import rock from "../../assets/images/batu.png";
import paper from "../../assets/images/kertas.png";
import scissors from "../../assets/images/gunting.png";
import "bootstrap/dist/css/bootstrap.css";
import { Link, useLocation } from "react-router-dom";
import "../App.css";
import { NotFound } from "../AccessAllPages";

function P1vsP2() {
  const { state } = useLocation();
  // const data = location.state;
  if (state !== null) {
    console.log(state.value.player1Name);
    const [result, setResult] = useState(state.value.winner);
    const [player2Choice, setPlayer2Choice] = useState(
      state.value.player2Choice
    );
    const [player1Choice, setPlayer1Choice] = useState(
      state.value.player1Choice
    );

    const setRock = () => {
      setPlayer1Choice("rock");
    };
    const setPaper = () => {
      setPlayer1Choice("paper");
    };
    const setScissors = () => {
      setPlayer1Choice("scissors");
    };

    return (
      <div className="container-fluid bigContainer">
        <div className="playerVsComContainer left-container">
          <div className="playerChoiceContainer">
            <h3 className="playerTitle title fs-3 mb-4">
              {state.value.player1Name}
            </h3>
            <Link
              className={
                player1Choice === "rock" ? "choiced" : "player1 choices"
              }
              style={
                player1Choice !== ""
                  ? { pointerEvents: "none" }
                  : { pointerEvents: "auto" }
              }
              onClick={setRock}
            >
              <img src={rock} alt="rock" className="rock" />
            </Link>
            <Link
              className={
                player1Choice === "paper" ? "choiced" : "player1 choices"
              }
              style={
                player1Choice !== ""
                  ? { pointerEvents: "none" }
                  : { pointerEvents: "auto" }
              }
              onClick={setPaper}
            >
              <img src={paper} alt="paper" className="paper" />
            </Link>
            <Link
              className={
                player1Choice === "scissors" ? "choiced" : "player1 choices"
              }
              style={
                player1Choice !== ""
                  ? { pointerEvents: "none" }
                  : { pointerEvents: "auto" }
              }
              onClick={setScissors}
            >
              <img src={scissors} alt="scissors" className="scissors" />
            </Link>
          </div>
          <div className="resultContainer">
            <h3 className="result title text-center">
              {result === null ? "Chose your choice" : `${result} win!`}
            </h3>
          </div>
          <div className="playerChoiceContainer">
            <h3 className="playerTitle title fs-3 text-transform-uppercase text-center mb-0">
              {result === null
                ? "Waiting for player 2 ..."
                : `${state.value.player2Name}`}
            </h3>
            <div
              className={player2Choice === "rock" ? "choiced" : "comChoices"}
            >
              <img src={rock} alt="rock" className="rock" />
            </div>
            <div
              className={player2Choice === "paper" ? "choiced" : "comChoices"}
            >
              <img src={paper} alt="paper" className="paper" />
            </div>
            <div
              className={
                player2Choice === "scissors" ? "choiced" : "comChoices"
              }
            >
              <img src={scissors} alt="scissors" className="scissors" />
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return <NotFound />;
  }
}

export default P1vsP2;
