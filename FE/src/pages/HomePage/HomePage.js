import React from "react";
import "../App.css";
// import { Link } from "react-router-dom";

function HomePage() {
  return (
    <div className="HomePage">
      <nav>
        <h3 className="logoHome">
          <i>ROCK PAPER SCISSOR</i>
        </h3>

        <ul className="navbar1">
          <li>
            <a href="Register">REGISTER</a>
          </li>
          <li>
            <a href="Login">LOGIN</a>
          </li>
          {/* <li>
            <a href="LobbyGame">PLAY</a>
          </li> */}
        </ul>
      </nav>
      <div className="textCenter">
        <h1 className="textAtas">Let's </h1>
        <h2 className="textBawah">PLAY THE GAMES</h2>
      </div>
      {/* <div className="tombolPlay">
        <div className="tombolPlay1">PLAY</div>
      </div> */}
    </div>
  );
}

export default HomePage;
