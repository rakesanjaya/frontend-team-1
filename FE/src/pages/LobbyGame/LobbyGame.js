/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import profile from "../../assets/icons/profile-icon.svg";
import axios from "axios";
import jwt_decode from "jwt-decode";
import "bootstrap/dist/css/bootstrap.css";
import "../App.css";

function LobbyGame() {
  // eslint-disable-next-line no-unused-vars
  const [roomsData, setRoomsData] = useState([]);
  const [errMsg, setErrMsg] = useState("");
  const [loading, setLoading] = useState(true);
  const [value, setValue] = useState(false);
  const [biodata, setBiodata] = useState({
    fullname: null,
    address: null,
    phoneNumber: null,
    dateOfBirth: null,
  });
  const [inputBiodata, setInputBiodata] = useState({
    fullname: null,
    address: null,
    phoneNumber: null,
    dateOfBirth: null,
  });

  const accessToken =
    "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwidXNlcm5hbWUiOiJiYWtodGlhciIsImNyZWF0ZWRBdCI6IjIwMjMtMDYtMjVUMTA6NDY6NTguNjI4WiIsInVwZGF0ZWRBdCI6IjIwMjMtMDYtMjVUMTA6NDY6NTguNjI4WiIsInJvbGUiOiJwbGF5ZXIiLCJpYXQiOjE2ODc5NTU5MDEsImV4cCI6MTY4ODA0MjMwMX0.sHy9_fB37ZxQCA3X8HHXXtUUC93nwDYkCgJ9UgLo5yU";
  const authorization = accessToken.split(" ")[1];
  const token = jwt_decode(authorization);

  const getAllRooms = async () => {
    try {
      const data = await axios
        .get("https://backend-team-1-five.vercel.app/all_rooms", {
          headers: { Authorization: `${accessToken}` },
        })
        .then((response) => {
          return response.data.message;
        });
      const restructuredData = [];
      data.map((roomData) => {
        return restructuredData.push({
          roomId: roomData.roomId,
          roomName: roomData.roomName,
          player1Id: roomData.player1Games.player1Id,
          player1Name: roomData.player1Games.player1Name,
          player1Status: roomData.resultGames?.[0],
          player2Id: roomData.player2Games?.player2Id,
          player2Name: roomData.player2Games?.player2Name,
          player2Status: roomData.resultGames?.[1],
        });
      });
      setLoading(false);
      setRoomsData(restructuredData);
    } catch (error) {
      console.log(error);
    }
  };

  const getBiodata = async () => {
    try {
      const biodatas = await axios
        .get(
          `https://backend-team-1-five.vercel.app/profile/${token.id.toString()}`,
          {
            headers: { Authorization: `${accessToken}` },
          }
        )
        .then((response) => {
          return response.data.message;
        });
      setBiodata({
        ...biodata,
        fullname: biodatas.fullname,
        address: biodatas.address,
        phoneNumber: biodatas.phoneNumber,
        dateOfBirth: biodatas.dateOfBirth,
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getAllRooms();
    getBiodata();
  }, []);

  const editClick = () => {
    setValue(true);
  };
  const updateClick = async() => {
    try {
      const response = await axios({
        method: 'put',
        url: `https://backend-team-1-five.vercel.app/profile/${token.id.toString()}`,
        headers: { Authorization: `${accessToken}` }, 
        data: inputBiodata
      }) 
      setErrMsg(response.data.message)
      getBiodata()
      setValue(false);
    } catch (error) {
      console.log(error)
      if (!error?.response) {
        setErrMsg("No Server Response");
      } else{
        setErrMsg(error.response.data.message);
      }
    }
  };
  return (
    <div className="bigContainer">
      <div className="left-container">
        <div className="pvcGame">
          <div className="playVCom">
            <Link className="btnVCom" to="/playervscom">
              Player Vs Com
            </Link>
          </div>
          <div className="playVCom">
            <Link className="btnVCom" to="/p1vsp2">
              Create Room PVP
            </Link>
          </div>
        </div>
        <div className="roomTitle title mx-4">Rooms : </div>
        <div className="roomContainer">
          {roomsData !== null ? (
            <div className="row ms-3">
              {loading !== true ? (
                roomsData.map((roomData) => {
                  return (
                    <div className="dataCourier" key={roomData.roomId}>
                      <Link
                        className="courierText"
                        to={"/p1vsp2"}
                        state={{ value: roomData}}
                      >
                        <>Room: {roomData.roomName}</>
                        <br></br>
                        <div className="">
                          Status:{" "}
                          {roomData.player2Name !== undefined
                            ? "closed"
                            : "open"}
                        </div>
                      </Link>
                    </div>
                  );
                })
              ) : (
                <>Loading...</>
              )}
            </div>
          ) : (
            <>Room Not Found</>
          )}
        </div>
      </div>
      <div className="right-container px-4">
        <div className="upper-right-container mt-1 mx-4">
          <div className="top-upper-right-container mt-2 pe-2">
            <Link
              className={
                value !== true
                  ? "gameHistoryText text-dark text-center text-decoration-underline me-4"
                  : "gameHistoryText text-dark text-center text-decoration-underline me-5 ms-0"
              }
              to={"/gamehistory"}
            >
              Game History
            </Link>
            <i
              className={value !== true ? "editIcons fa fa-pencil" : null}
              aria-hidden="true"
              onClick={editClick}
            />
          </div>
          <img src={profile} alt="profile" className="rounded-circle mt-1" />
          <div className="profileTitle title">Player's name</div>
        </div>
        <div
          className={
            value !== true
              ? "updatedContainer lower-right-container"
              : "lower-right-container row mt-2 pb-5 pt-2 ps-4"
          }
        >
          <div className="biodata">
            Fullname: {biodata.fullname === null ? "....." : biodata.fullname}
          </div>
          <div className="biodata">
            Address: {biodata.address === null ? "....." : biodata.address}
          </div>
          <div className="biodata">
            Phone:{" "}
            {biodata.phoneNumber === null ? "....." : biodata.phoneNumber}
          </div>
          <div className="biodata">
            Date of Birth:{" "}
            {biodata.dateOfBirth === null ? "....." : biodata.dateOfBirth}
          </div>
        </div>
        <div
          className={
            value !== true
              ? "notUpdated lower-right-container-input"
              : "lower-right-container-input formBiodata mt-2 ps-5"
          }
        >
          <div className="commandInput text-center text-capitalize fw-regular fs-6 text-dark mb-1 me-5">
            Please input your biodata
          </div>
          <label htmlFor="fullnameInput" className="labelInput fs-6 mt-0">
            Fullname:
          </label>
          <input
            type="text"
            placeholder={
              biodata.fullname === null ? "Fullname" : `${biodata.fullname}`
            }
            className="inputBiodata inputData mt-0"
            id="fullnameInput"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, fullname: e.target.value });
            }}
          />
          <label htmlFor="addressInput" className="labelInput fs-6 mt-0">
            Address:
          </label>
          <input
            type="text"
            placeholder={
              biodata.address === null ? "Address" : `${biodata.address}`
            }
            className="inputBiodata inputData mt-0"
            id="addressInput"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, address: e.target.value });
            }}
          />
          <label htmlFor="phoneNumber" className="labelInput fs-6 mt-0">
            Phone Number:
          </label>
          <input
            type="text"
            placeholder={
              biodata.phoneNumber === null
                ? "Phone Number"
                : `${biodata.phoneNumber}`
            }
            className="inputBiodata inputData mt-0"
            id="phoneNumber"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, phoneNumber: e.target.value });
            }}
          />
          <label htmlFor="dateOfBrith" className="labelInput fs-6 mt-0">
            Date of Birth:
          </label>
          <input
            type="text"
            placeholder={
              biodata.dateOfBirth === null
                ? "Date of Birth"
                : `${biodata.dateOfBirth}`
            }
            className="inputBiodata inputData mt-0"
            id="dateOfBrith"
            disabled={false}
            onChange={(e) => {
              setInputBiodata({ ...inputBiodata, dateOfBirth: e.target.value });
            }}
          />
          <button
            className="updateBiodata button ms-5 py-3"
            onClick={updateClick}
          >
            update Biodata
          </button>
        </div>
        <div
                className={errMsg ? "errMsg" : "offscreen"}
                aria-live="assertive"
              >
                {errMsg}
              </div>

      </div>
    </div>
  );
}

export default LobbyGame;
