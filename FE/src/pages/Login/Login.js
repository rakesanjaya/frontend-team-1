import React, { useState, useRef } from "react";
import Title from "../../components/Title";
import mylogin from "../../assets/images/loginImage.jpg";
import "bootstrap/dist/css/bootstrap.css";
import { Link, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import { loginSchema } from "../../schemas";
import axios from "axios";
import "../App.css";

function Login() {
  // const [user, setUser] = useState({ username: "", password: "" });
  // const [isErrors, setIsErrors] = useState(true);
  // initialize login schema using formik
  const [errMsg, setErrMsg] = useState("");
  const navigate = useNavigate();
  const { values, errors, handleChange, handleSubmit, touched } = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    // validation schema using yup
    validationSchema: loginSchema,
    onSubmit: async (values, helpers) => {
      try {
        // hit API
        const response = await axios.post(
          "https://backend-team-1-five.vercel.app/login",
          values
        );
        const token = response.data.accessToken;
        localStorage.setItem("accessToken", token);
        navigate("/lobbygame");
        helpers.resetForm({ values });
      } catch (err) {
        console.log(err);
        if (!err?.response) {
          setErrMsg("No Server Response");
        } else if (err.response?.status === 400) {
          setErrMsg("Invalid Credentials");
        }
      }
    },
  });

  return (
    <div className="container-fluid loginContainer">
      <div className="container">
        <div className="row content">
          <div className="col-md-6 mb-3">
            <img src={mylogin} alt="login " className="img-fluid loginImage" />
          </div>
          <div className="col-md-6 ">
            <Title classProps="title text-center" children="Sign In" />
            <form className="form" id="form1" onSubmit={handleSubmit}>
              <div className="form-group field-holder">
                <label
                  className={values.username && "filled"}
                  htmlFor="loginUsername"
                >
                  Username
                </label>
                <input
                  type="text"
                  id="loginUsername"
                  className="form-control input"
                  name="username"
                  value={values.username}
                  onChange={handleChange}
                />
              </div>
              <div
                className={
                  touched.username && errors.username ? "" : "errorsMessage"
                }
              >
                {errors.username}
              </div>
              <div className="form-group field-holder">
                <label
                  className={values.password && "filled"}
                  htmlFor="loginPassword"
                >
                  Password
                </label>
                <input
                  type="password"
                  id="loginPassword"
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  className="form-control input"
                />
              </div>
              <div
                className={
                  touched.password && errors.password ? "" : "errorsMessage"
                }
              >
                {errors.password}
              </div>
              <div
                className={errMsg ? "errMsg" : "offscreen"}
                aria-live="assertive"
              >
                {errMsg}
              </div>
              <div className="text-center">
                <button className="btn button" type="submit">
                  Sign In
                </button>
                <p className="registerLink">
                  New member? <Link to="/register">Create Account</Link>
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
